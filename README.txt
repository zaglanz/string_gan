Ashwin & Zach 665 project README:

If you get errors from either file perhaps delete __pycache__ and .vector_cache

PREREQUISATES:
	torch
	torchtext
	numpy
	spaCy (english)

To create a dataset:
	place each input sequence, zork output, rooms.c.gcov and nrooms.c.gcov in a folder for each sample
	input sequences are in txt format with one command per line
	
	modify path var at top of collect_data.py to the path containing these folders
	2 supported formats:
		all sample folders in path (use zork_data_steps)
			see example path 'gen_coverage'
			use zork_dataset for myset in collect_data (default)
			

		all sample folders in path, with subfolders for each step, containing all info for that step.
			see example path 'coverage'
			use zork_dataset_steps (comment/uncomment my_set declaration in collect_data.py)
			there should be a file '665_data.txt' at path, with format:
				<folder_name> <n_steps>
				on each line, for each input sequence with n steps
			view example path 'coverage_steps'

	at the very end of collect_data.py (EXPORT section) set file paths for exported data:
		Vocab list, input data, input lengths, label data

	run collect_data.py
	this should create four files containing that information

to train and test model:
	at top of main_665.py:
		modify paths to collect_data output if changed
		modify gen/discriminator file paths
			used if you want to load models without training, comments mark what to uncomment if not training.
			by default models are trained and not loaded
		modify generated sample path (and filename):
			128 same sequences will be generated
			a number and '.txt' will be appended and files saved
		any hyperparameters desired

	to modify reward weights for metrics:
		edit weights in models_665.py -> generator -> batchPGLoss

	run main_665.py
	by default, models will be trained (and saved) and should output samples to generated_inputs folder