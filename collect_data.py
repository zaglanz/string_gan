import torch
from torchtext import data
from torchtext import datasets
import random
import time
import os
import copy
import csv
import numpy as np
import torch.nn.utils.rnn as rnn_utils
from torch.utils.data.sampler import SubsetRandomSampler

path = os.path.join("..", "Coverage")                                    #PATH TO GAME FOLDERS, MODIFY 
game_lower_index = 31
game_upper_index = 130

class zork_dataset_steps(torch.utils.data.Dataset):
    
    def __init__(self, folder_path):
        self.path = folder_path
        ds_path = os.path.join(folder_path, "665_data.txt")
        ds_f = open(ds_path, "r")
        ds_lines = ds_f.readlines()
        self.game_ns = []
        self.step_ns = []
        for x in ds_lines:
            xsp = x.split()
            
            #info for retrieval
            tmp_game = xsp[0][4:]
            #for i in range(int(xsp[1])+1,int(xsp[1])+1):
            for i in range(500,501):
                self.game_ns.append(tmp_game)
                self.step_ns.append(str(i))
                
                #parse and store labels
                full_path = os.path.join(folder_path, xsp[0], "Step"+str(i))

                info_paths = []
                info_paths.append(os.path.join(full_path, "rooms.c.gcov")) # CHANGE if i want more files
                info_paths.append(os.path.join(full_path, "nrooms.c.gcov")) # CHANGE if i want more files
                label = self.get_coverage(info_paths) # grab coverage information from files
                coverage_score = 0.0
                for i in label:
                    if i > 0.5:
                        coverage_score += 1.0
                coverage_score = coverage_score / len(label)
                
                #print(coverage_score)
                output_validity = self.parse_output(os.path.join(full_path, "ZorkOut.gameOut"))
                #print(output_validity)
                
                full_label = []
                full_label.append(1.0)
                full_label.append(output_validity)
                full_label.append(coverage_score)
                
                label_path = os.path.join(full_path, "label.csv")
                with open(label_path, 'w', newline='') as myfile:
                    wr = csv.writer(myfile)
                    wr.writerow(full_label)

        self.len = len(self.step_ns)
        
    def __getitem__(self, index):
        item_path = os.path.join(self.path, "Game"+self.game_ns[index], "Step"+self.step_ns[index])
        input_path = os.path.join(item_path, "input")
        label_path = os.path.join(item_path, "label.csv")
        x = np.loadtxt(input_path, delimiter='\n', dtype='str')
        x = x.tolist()
        y = np.loadtxt(label_path, delimiter=',', dtype=np.float32)  
        return x, y
    
    def __len__(self):
        return self.len
    
    def get_coverage(self, filepaths):
        label = []
        for filepath in filepaths:
            info_f = open(filepath, "r")
            info_lines = info_f.readlines()
            i = 0
            for y in info_lines:
                i += 1
                temp_str = y.rstrip().split()[0][0:-1] #grab line count - ':'
                if temp_str == '-':
                    continue
                if temp_str == '#####':
                    label.append(0)
                else:
                    label.append(int(temp_str.replace("*","")))
        return label
    
    def parse_output(self, filepath):
        info_f = open(filepath, "r")
        info_lines = info_f.readlines()
        i = 0
        total = 0
        error_msgs = {">I can't see one here.", ">You can't go that way.", 
                      ">The window is closed.", ">I could't find anything.", 
                      ">You are empty handed.", ">You are in perfect health.",
                     ">You cannot climb any higher."}
        for y in info_lines:
            ystr = y.rstrip()
            if len(ystr) and ystr[0] == '>':
                total += 1
                if(not (ystr in error_msgs)):
                    i += 1
        return i / total
    
class zork_dataset(torch.utils.data.Dataset):
    
    def __init__(self, folder_path, low_bound, high_bound):
        self.path = folder_path
        self.game_ns = []
        self.step_ns = []
        for i in range(low_bound,high_bound+1):
         #   self.game_ns.append(tmp_game)
            self.step_ns.append(str(i))

            #parse and store labels
            full_path = os.path.join(folder_path, "Game"+str(i))

            info_paths = []
            info_paths.append(os.path.join(full_path, "rooms.c.gcov")) # CHANGE if i want more files
            info_paths.append(os.path.join(full_path, "nrooms.c.gcov")) # CHANGE if i want more files
            label = self.get_coverage(info_paths) # grab coverage information from files
            coverage_score = 0.0
            for i in label:
                if i > 0.5:
                    coverage_score += 1.0
            coverage_score = coverage_score / len(label)

            #print(coverage_score)
            output_validity = self.parse_output(os.path.join(full_path, "ZorkOut.gameOut"))
            #print(output_validity)

            full_label = []
            full_label.append(1.0)
            full_label.append(output_validity)
            full_label.append(coverage_score)

            label_path = os.path.join(full_path, "label.csv")
            with open(label_path, 'w', newline='') as myfile:
                wr = csv.writer(myfile)
                wr.writerow(full_label)

        self.len = len(self.step_ns)
        
    def __getitem__(self, index):
        item_path = os.path.join(self.path, "Game"+self.step_ns[index])
        input_path = os.path.join(item_path, "input")
        label_path = os.path.join(item_path, "label.csv")
        x = np.loadtxt(input_path, delimiter='\n', dtype='str')
        x = x.tolist()
        y = np.loadtxt(label_path, delimiter=',', dtype=np.float32)  
        return x, y
    
    def __len__(self):
        return self.len
    
    def get_coverage(self, filepaths):
        label = []
        for filepath in filepaths:
            info_f = open(filepath, "r")
            info_lines = info_f.readlines()
            i = 0
            for y in info_lines:
                i += 1
                temp_str = y.rstrip().split()[0][0:-1] #grab line count - ':'
                if temp_str == '-':
                    continue
                if temp_str == '#####':
                    label.append(0)
                else:
                    label.append(int(temp_str.replace("*","")))
        return label
    
    def parse_output(self, filepath):
        info_f = open(filepath, "r")
        info_lines = info_f.readlines()
        i = 0
        total = 0
        error_msgs = {">I can't see one here.", ">You can't go that way.", ">The window is closed.",
                      ">I could't find anything.", ">You are empty handed.", ">You are in perfect health.",
                     ">You cannot climb any higher."}
        for y in info_lines:
            ystr = y.rstrip()
            if len(ystr) and ystr[0] == '>':
                total += 1
                if(not (ystr in error_msgs)):
                    i += 1
        return i / total
    
#my_set = zork_dataset_steps(path)
my_set = zork_dataset(path, 31, 130)                                                                  #change depending on file format
train_len = int(len(my_set) * 0.7)
test_len = int(len(my_set) * 0.15)
val_len = len(my_set) - train_len - test_len

train_data, test_data, val_data = torch.utils.data.random_split(my_set, (train_len, test_len, val_len))
SEED = 1234
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
TEXT = data.Field(tokenize = 'spacy')
LABEL = data.LabelField()
batch_size = 64
validation_split = .2
shuffle_dataset = True
random_seed= 42

# Creating data indices for training and validation splits:
dataset_size = len(my_set)
indices = list(range(dataset_size))
split = int(np.floor(validation_split * dataset_size))
if shuffle_dataset :
    np.random.seed(random_seed)
    np.random.shuffle(indices)
train_indices, val_indices = indices[split:], indices[:split]
train_sampler = SubsetRandomSampler(train_indices)
valid_sampler = SubsetRandomSampler(val_indices)

train_loader = torch.utils.data.DataLoader(my_set, batch_size=batch_size, 
                                           sampler=train_sampler)
validation_loader = torch.utils.data.DataLoader(my_set, batch_size=batch_size,
                                                sampler=valid_sampler)

#print(type(train_loader))
MAX_VOCAB_SIZE = 25_000
list1, list2 = zip(*train_data) #TODO convert from u14 to something else?
#print(len(list2[0]))                       
TEXT.build_vocab(list1,                                                            ## get vocab from samples
                 max_size = MAX_VOCAB_SIZE, 
                 vectors = "glove.6B.100d", 
                 unk_init = torch.Tensor.normal_)

#LABEL.build_vocab(list1)  #TODO replace references to label with #s
vocab_list = []
for i in range(0,len(TEXT.vocab)):
    vocab_list.append(TEXT.vocab.itos[i])
    #print(TEXT.vocab.itos[i])
print(vocab_list)
        
def my_stoi(TEXT, x):
    return TEXT.vocab.stoi[x]

int_list = []
m_lengths = []
for inputs, labels in my_set:                                                       ## gets one-hot vectors
    t_list = []                                                                     ## also gets seq lengths for packing
    counter = 0
    for i in inputs:
        t_list.append(my_stoi(TEXT,i))
        counter += 1
    int_list.append(torch.LongTensor(t_list))
    m_lengths.append(counter)
#print(len(int_list))
#padded = rnn_utils.pack_sequence(int_list, enforce_sorted=False)
padded = rnn_utils.pad_sequence(int_list, True, padding_value = 1)                  ## pads sequences with 1 '<PAD>' to be the same length (max length)
lengths_tensor = torch.LongTensor(m_lengths)


## EXPORT                                                                           ## CHANGE FILES NAMES AS NEEDED
print("Exporting files...") 

with open('zork_vocab.txt', 'w') as vocab_file:  ##VOCAB LIST                                   ## save vocab to file
    for word in vocab_list:
        vocab_file.write('%s\n' % word)

torch.save(padded, 'zorkdata_long_20_60.pt')  ##INPUT DATA                                         
torch.save(lengths_tensor, 'zorkdata_lengths_20_60.pt') #INPUT LENGTHS     ## export encoded sequences & lengths to separate files
#packed_copy = torch.load('zorkdata_long_20_60.pt')
list2 = []
for inputs, labels in my_set:
    list2.append(labels)
target_tensor = torch.FloatTensor(list2)
torch.save(target_tensor, 'zorkdata_long_20_60_target.pt') #LABEL DATA   ## export labels to file 

print("Export complete.")